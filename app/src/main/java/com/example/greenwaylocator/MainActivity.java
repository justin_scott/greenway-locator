/* Greenway Locator
*  Group 11
*  MainActivity.java
*  This app allows anyone in the UNCC area to find, navigate, and workout at multiple greenways in the area
* */
package com.example.greenwaylocator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    // Variables Initialized
    Button btLocation;
    Button btMap;
    TextView textView1, textView2;
    FusedLocationProviderClient fusedLocationProviderClient;
    Double mapLatitude, mapLongitude;
    // Arrays initialized
    String[] greenway_names;
    TypedArray preview_pics;
    String[] latitude;
    String[] longitude;
    String[] address;

    List<Greenway> greenways;
    ListView mylistview;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        btLocation = findViewById(R.id.bt_location);
        btMap = findViewById(R.id.button4);
        textView1 = findViewById(R.id.text_view1);
        textView2 = findViewById(R.id.text_view2);

        bottomNavigationView = findViewById(R.id.nav_bar);

        bottomNavigationView.setSelectedItemId(R.id.nav_greenways);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            // This method is used to navigate between activities in the nav bar
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_health:
                        startActivity(new Intent(getApplicationContext()
                        ,HealthActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_workouts:
                        startActivity(new Intent(getApplicationContext()
                                ,WorkoutActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_tracker:
                        startActivity(new Intent(getApplicationContext()
                                ,TrackerActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        // Used to open the map activity on button press
        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMapLocation();
            }
        });

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        btLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this
                        , Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this
                        , Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this
                            , new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
                }
            }
        });
        // Populates the greenway arraylist with greenway objects
        greenways = new ArrayList<Greenway>();
        greenway_names = getResources().getStringArray(R.array.Greenway_names);
        preview_pics = getResources().obtainTypedArray(R.array.preview_pics);
        latitude = getResources().getStringArray(R.array.Latitudes);
        longitude = getResources().getStringArray(R.array.Longitudes);
        address = getResources().getStringArray(R.array.Greenway_Addresses);

        for (int i = 0; i < greenway_names.length; i++) {
            Greenway item = new Greenway(greenway_names[i], preview_pics.getResourceId(i, -1), latitude[i], longitude[i], address[i]);
            greenways.add(item);
        }

        // Calls the CustomAdapter class which is used to display greenways in the listview
        mylistview = (ListView) findViewById(R.id.list);
        CustomAdapter adapter = new CustomAdapter(this, greenways);
        mylistview.setAdapter(adapter);

        mylistview.setOnItemClickListener(this);
    }

    // When a greenway is clicked, this method sends the relevant information to google maps for navigation
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String greenwayAddress = greenways.get(position).getAddress();
        String latitude = greenways.get(position).getLatitude();
        String longitude = greenways.get(position).getLongitude();
        // Prepares URL for google maps with the address
        Uri IntentUri = Uri.parse("google.navigation:q=" + greenwayAddress);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, IntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    // This method only runs if the user denies location permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults.length > 0 && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            getLocation();
        } else {
            AlertDialog alt = new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Permission Needed!")
                    .setMessage("Please allow Greenway Locator access to your location. You may need to access location settings in the Settings app.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(getApplicationContext()
                                    ,MainActivity.class));
                        }
                    })
                    .create();
            alt.show();
        }
    }

    // This method gets the users location via coordinates
    @SuppressLint("MissingPermission")
    private void getLocation() {
        // Location Manager is property of google and implemented in the build.gradle file (Module: app)
        LocationManager locationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE
        );

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        // Displays users coordinates in the text views
                        textView1.setText(String.valueOf(location.getLatitude()));
                        textView2.setText(String.valueOf(location.getLongitude()));
                    }else{
                        // Note: This is a common way to get the location and is necessary in order to get the location ASAP
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);

                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                Location location1 = locationResult.getLastLocation();
                                textView1.setText(String.valueOf(location1.getLatitude()));
                                textView2.setText(String.valueOf(location1.getLongitude()));
                            }
                        };
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                                locationCallback, Looper.myLooper());
                    }

                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
    // This method is used in a similar way to the getLocation method but instead this method sends the users coordinates
    // to the MapActivity
    @SuppressLint("MissingPermission")
    private void getMapLocation() {
        // Location Manager is property of google and implemented in the build.gradle file (Module: app)
        LocationManager locationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE
        );

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        mapLatitude = (location.getLatitude());
                        mapLongitude = (location.getLongitude());

                        // Puts the users coordinates in the intent and starts the MapActivity
                        Intent i = new Intent(MainActivity.this, MapActivity.class);
                        i.putExtra("lat", mapLatitude);
                        i.putExtra("lon", mapLongitude);
                        startActivity(i);
                    }else{
                        // Note: This is a common way to get the location and is necessary in order to get the location ASAP
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);

                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                Location location1 = locationResult.getLastLocation();
                                mapLatitude = (location1.getLatitude());
                                mapLongitude = (location1.getLongitude());
                            }
                        };
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                                locationCallback, Looper.myLooper());
                    }

                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}