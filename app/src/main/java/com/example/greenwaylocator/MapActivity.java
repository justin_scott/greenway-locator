/*  Greenway Locator
 *  Group 11
 *  MapActivity.java
 *  This class is used to display a world map with each Greenway displayed in a pin along with the users location
 * */
package com.example.greenwaylocator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap map;
    Double myLat, myLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Receiving location information sent from the MainActivity
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            myLat = extras.getDouble("lat");
            myLong = extras.getDouble("lon");
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        // Declaring each location with their specific coordinates
        LatLng MyLocation = new LatLng(myLat, myLong);
        LatLng TobyCreek = new LatLng(35.297130, -80.740710);
        LatLng BartonCreek = new LatLng(35.312290, -80.748100);
        LatLng MallardCreek = new LatLng(35.320540, -80.731290);
        LatLng KirkFarm = new LatLng(35.320540, -80.731290);
        LatLng ClarksCreek = new LatLng(35.334260, -80.773800);
        LatLng Hector = new LatLng(35.364009, -80.700305);
        LatLng LittleSugar = new LatLng(35.2120473, -80.835746);
        LatLng BriarCreek = new LatLng(35.2186731, -80.795419);

        // Adding each marker to the map
        map.addMarker(new MarkerOptions().position(MyLocation).title("Me")).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        map.addMarker(new MarkerOptions().position(TobyCreek).title("Toby Creek Greenway"));
        map.addMarker(new MarkerOptions().position(BartonCreek).title("Barton Creek Greenway"));
        map.addMarker(new MarkerOptions().position(MallardCreek).title("Mallard Creek Greenway"));
        map.addMarker(new MarkerOptions().position(KirkFarm).title("Kirk Farm Fields"));
        map.addMarker(new MarkerOptions().position(ClarksCreek).title("Clarks Creek Greenway"));
        map.addMarker(new MarkerOptions().position(Hector).title("Hector H Henry II Greenway"));
        map.addMarker(new MarkerOptions().position(LittleSugar).title("Little Sugar Creek Greenway"));
        map.addMarker(new MarkerOptions().position(BriarCreek).title("Briar Creek Greenway"));

        map.moveCamera(CameraUpdateFactory.newLatLng(MyLocation));
    }
}