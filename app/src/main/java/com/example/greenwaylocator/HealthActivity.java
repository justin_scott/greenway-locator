/*  Greenway Locator
 *  Group 11
 *  HealthActivity.java
 *  This class is used to get the users weight and pass it to the Workout and Tracker Activites
 * */
package com.example.greenwaylocator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HealthActivity extends AppCompatActivity {
    // Initialize variables
    BottomNavigationView bottomNavigationView;
    EditText weight;
    Button next;
    String userWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_health);
        weight = findViewById(R.id.weightInput);
        next = findViewById(R.id.nextMain);

        bottomNavigationView = findViewById(R.id.nav_bar);

        bottomNavigationView.setSelectedItemId(R.id.nav_health);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_greenways:
                        startActivity(new Intent(getApplicationContext()
                                ,MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_workouts:
                        startActivity(new Intent(getApplicationContext()
                                ,WorkoutActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_tracker:
                        startActivity(new Intent(getApplicationContext()
                                ,TrackerActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        next.setOnClickListener(new View.OnClickListener()
        {
            // Sends information to the Workout Activity
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(HealthActivity.this, WorkoutActivity.class);
                userWeight = weight.getText().toString();
                i.putExtra("weight",userWeight);
                startActivity(i);
                finish();
            }
        });
    }
}